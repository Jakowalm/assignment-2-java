package no.problem.Assignment2.controllers;

import no.problem.Assignment2.databasemanager.DatabaseManager;
import no.problem.Assignment2.models.TrackSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ThymeleafController {

    @Autowired
    DatabaseManager dbManager;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("tracks", dbManager.getHomePageContent());
        model.addAttribute("track", new TrackSearch(""));
        return "home";
    }

    @PostMapping("/")
    public String home(@ModelAttribute TrackSearch track, BindingResult error, Model model) {
        return "home";
    }

    @GetMapping("/search")
    public String search(Model model) {
        return "search";
    }
    @PostMapping("/search")
    public String search(@ModelAttribute TrackSearch trackName, BindingResult error, Model model) {
        model.addAttribute("track", dbManager.getTrackInfo(trackName.getSearchText()));
        return "search";
    }
}
