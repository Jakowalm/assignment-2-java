package no.problem.Assignment2.controllers;

import no.problem.Assignment2.models.*;
import no.problem.Assignment2.databasemanager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class CustomerController {

    @Autowired
    DatabaseManager dbManager;

    @GetMapping("/api/customer/all")
    public ArrayList<Customer> getAllCustomers() {
        return dbManager.getAllCustomers(0, 0);
    }

    @GetMapping("/api/customer/id/{id}")
    public Customer getSingleCustomerById(@PathVariable int id) {
        return dbManager.getSingleCustomerById(id);
    }

    @GetMapping("/api/customer/name/{firstName}/{lastName}")
    public Customer getSingleCustomerByName(@PathVariable String firstName, @PathVariable String lastName) {
        return dbManager.getSingleCustomerByName(firstName,lastName);
    }

    @GetMapping("/api/customer/all/offset/{limit}/{offset}")
    public ArrayList<Customer> getCustomersWithLimitAndOffset(@PathVariable int limit, @PathVariable int offset) {
        return dbManager.getAllCustomers(limit, offset);
    }

    @GetMapping("/api/customer/insert/{firstName}/{lastName}/{country}/{postalCode}/{phone}/{email}")
    public boolean insertSingleCustomer(
            @PathVariable String firstName,
            @PathVariable String lastName,
            @PathVariable String country,
            @PathVariable String postalCode,
            @PathVariable String phone,
            @PathVariable String email) {
        Customer toBeInserted = new Customer(-99, firstName,lastName,country,postalCode,phone,email);
        return dbManager.insertSingleCustomer(toBeInserted);
    }

    @GetMapping("/api/customer/update/{id}/{firstName}/{lastName}/{country}/{postalCode}/{phone}/{email}")
    public boolean updateSingleCustomer(
            @PathVariable int id,
            @PathVariable String firstName,
            @PathVariable String lastName,
            @PathVariable String country,
            @PathVariable String postalCode,
            @PathVariable String phone,
            @PathVariable String email) {
        Customer toBeInserted = new Customer(id, firstName,lastName,country,postalCode,phone,email);
        return dbManager.updateSingleCustomer(toBeInserted);
    }

    @GetMapping("/api/customer/countries")
    public ArrayList<CustomerCountry> getCountriesByCustomerCount() {
        return dbManager.getCountriesByCustomerCount();
    }

    @GetMapping("/api/customer/all/spending")
    public ArrayList<CustomerSpender> getCustomersBySpending() {
        return dbManager.getCustomersBySpending();
    }

    @GetMapping("/api/customer/id/{id}/genres")
    public ArrayList<CustomerGenre> getTopCustomerGenre(@PathVariable int id) {
        return dbManager.getTopCustomerGenre(id);
    }
}
