package no.problem.Assignment2.models;

public class CustomerGenre {
    private String name;
    private int numberOfInvoices;

    public CustomerGenre(String name, int numberOfInvoices) {
        this.name = name;
        this.numberOfInvoices = numberOfInvoices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfInvoices() {
        return numberOfInvoices;
    }

    public void setNumberOfInvoices(int numberOfInvoices) {
        this.numberOfInvoices = numberOfInvoices;
    }

    @Override
    public String toString() {
        return "CustomerGenre{" +
                "name='" + name + '\'' +
                ", numberOfInvoices=" + numberOfInvoices +
                '}';
    }
}
