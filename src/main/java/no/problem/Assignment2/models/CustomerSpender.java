package no.problem.Assignment2.models;

public class CustomerSpender {
    int id;
    double totalSpending;

    public CustomerSpender(int id, double totalSpending) {
        this.id = id;
        this.totalSpending = totalSpending;
    }

    @Override
    public String toString() {
        return "CustomerSpender{" +
                "id=" + id +
                ", totalSpending=" + totalSpending +
                '}';
    }
}
