package no.problem.Assignment2.models;

public class TrackSearch {
    public String searchText;

    public TrackSearch(String searchText) {
        this.searchText = searchText;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
}
