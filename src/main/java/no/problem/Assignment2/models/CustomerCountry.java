package no.problem.Assignment2.models;

public class CustomerCountry {
    String name;
    int numberOfCustomers;

    public CustomerCountry(String name, int numberOfCustomers) {
        this.name = name;
        this.numberOfCustomers = numberOfCustomers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    @Override
    public String toString() {
        return "CustomerCountry{" +
                "name='" + name + '\'' +
                ", numberOfCustomers=" + numberOfCustomers +
                '}';
    }
}
