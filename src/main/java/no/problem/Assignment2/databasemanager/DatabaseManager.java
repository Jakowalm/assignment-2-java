package no.problem.Assignment2.databasemanager;

import no.problem.Assignment2.models.*;

import java.util.ArrayList;

public interface DatabaseManager {
    public ArrayList<Customer> getAllCustomers(Integer limit, Integer offset);
    public Customer getSingleCustomerById(int id);
    public Customer getSingleCustomerByName(String fName, String lName);
    public boolean insertSingleCustomer(Customer customer);
    public boolean updateSingleCustomer(Customer customer);
    public ArrayList<CustomerCountry> getCountriesByCustomerCount();
    public ArrayList<CustomerSpender> getCustomersBySpending();
    public ArrayList<CustomerGenre> getTopCustomerGenre(int customerId);
    public ArrayList<Track> getHomePageContent();
    public Track getTrackInfo(String trackName);
}
