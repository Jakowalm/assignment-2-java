package no.problem.Assignment2.databasemanager;

import no.problem.Assignment2.models.*;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

@Repository
@SuppressWarnings({"finally", "ReturnInsideFinallyBlock", "ThrowablePrintedToSystemOut"})
public class DatabaseManagerImpl implements DatabaseManager {

    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    /**
     * This method starts from a given point and finds a set limit of customers starting there.
     * If the inputs are 0, the method will return an array with all the customers in the database.
     *
     * @param limit how many objects should be returned
     * @param offset what index the search should begin at
     * @return an array with all the customers
     */
    public ArrayList<Customer> getAllCustomers(Integer limit, Integer offset) {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            String statement;
            PreparedStatement preparedStatement;
            if (limit == 0) {
                statement = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email " +
                        "FROM customer";
                preparedStatement =
                        conn.prepareStatement(statement);
            } else {
                statement = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email " +
                        "FROM customer " +
                        "LIMIT ? " +
                        "OFFSET ?";
                preparedStatement =
                        conn.prepareStatement(statement);
                preparedStatement.setInt(1, limit);
                preparedStatement.setInt(2, offset);
            }

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        }
        catch (Exception ex){
            System.out.println("Error occurred in getAllCustomers():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getAllCustomers():");
                System.out.println(ex);
            }
            return customers;
        }
    }

    /**
     * This method finds a single customer using its ID
     *
     * @return returns a single customer
     */
    public Customer getSingleCustomerById(int id) {
        Customer customer = null;

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email " +
                    "FROM customer as c " +
                    "WHERE c.CustomerId = ?";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );
        }
        catch (Exception ex){
            System.out.println("Something went wrong in getSingleCustomerById():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getSingleCustomerById():");
                System.out.println(ex);
            }
            return customer;
        }
    }

    /**
     * This method finds a single customer using its FirstName and LastName
     *
     * @return returns a single customer
     */
    public Customer getSingleCustomerByName(String fName, String lName) {
        if (fName == null || fName.equals("")) fName = "%";
        if (lName == null || lName.equals("")) lName = "%";
        Customer customer = null;

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email " +
                    "FROM customer as c " +
                    "WHERE c.FirstName LIKE ? AND  c.LastName LIKE ?";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            preparedStatement.setString(1, fName);
            preparedStatement.setString(2, lName);
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );
        }
        catch (Exception ex){
            System.out.println("Something went wrong in getSingleCustomerByName():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getSingleCustomerByName():");
                System.out.println(ex);
            }
            return customer;
        }
    }

    /**
     * This method takes in a single customer and inserts at the end of the database.
     *
     * @param customer The new customer object
     * @return whether the insertion was a success or not
     */
    public boolean insertSingleCustomer(Customer customer) {
        boolean success = true;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            String statement = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());

            preparedStatement.executeUpdate();
        }
        catch (Exception ex){
            System.out.println("Error occurred in insertSingleCustomer():");
            System.out.println(ex);
            success = false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in insertSingleCustomer():");
                System.out.println(ex);
                success = false;
            }
            return success;
        }
    }

    // TEMPORARY JACK WHACK METHOD for patching instead of updating. It works, but it's not very efficient.
    // Left it in case you want to have a look :)
    /*public boolean patchSingleCustomer(int id, HashMap<String, String> updateMap) {
        boolean success = true;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            for(String attribute: updateMap.keySet()) {
                String statement = "UPDATE Customer ";
                statement += "SET " + attribute + " = ? ";
                statement += "WHERE CustomerId = ? ";
                PreparedStatement preparedStatement =
                        conn.prepareStatement(statement);
                preparedStatement.setString(1, updateMap.get(attribute));
                preparedStatement.setInt(2, id);
                preparedStatement.executeUpdate();
            }

        }
        catch (Exception ex){
            System.out.println("Error occurred in patchSingleCustomer():");
            System.out.println(ex);
            success = false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in patchSingleCustomer():");
                System.out.println(ex);
                success = false;
            }
            return success;
        }
    }*/

    /**
     * Takes in a customer object and updates the customer at its id.
     *
     * @param customer The updated customer object, should always have the same id as the customer to be updated.
     * @return whether the insertion was a success or not
     */
    public boolean updateSingleCustomer(Customer customer) {
        boolean success = true;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");
            String statement = "UPDATE Customer " +
                    "SET FirstName = ?, LastName = ?, Country = ?, PostalCode =  ?, Phone =  ?, Email = ? "+
                    "WHERE CustomerId = ?";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getCustomerId());

            preparedStatement.executeUpdate();
        }
        catch (Exception ex){
            System.out.println("Error occurred in updateSingleCustomer():");
            System.out.println(ex);
            success = false;
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in updateSingleCustomer():");
                System.out.println(ex);
                success = false;
            }
            return success;
        }
    }

    /**
     * A method that goes through all the Countries in the Customer table and adds their respective customer numbers together.
     *
     * @return an array with all the Countries sorted by customer amount.
     */
    public ArrayList<CustomerCountry> getCountriesByCustomerCount() {
        ArrayList<CustomerCountry> countries = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT COUNT(*) as myCount, Country " +
                    "FROM Customer c " +
                    "GROUP BY Country " +
                    "ORDER BY myCount DESC" ;
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countries.add(new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("myCount")
                ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong in getCountriesByCustomerCount():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getCountriesByCustomerCount():");
                System.out.println(ex);
            }
            return countries;
        }
    }

    /**
     * Gos through all the customers and adds together their spending by going through the Invoices.
     *
     * @return an array of all the Customers and their spending sorted descending.
     */
    public ArrayList<CustomerSpender> getCustomersBySpending() {
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT ROUND(SUM(i.Total), 2) as totalSpent, CustomerId " +
                    "FROM Invoice i NATURAL JOIN Customer c " +
                    "GROUP BY CustomerId " +
                    "ORDER BY totalSpent DESC" ;
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerSpenders.add(new CustomerSpender(
                        resultSet.getInt("CustomerId"),
                        resultSet.getDouble("totalSpent")
                ));
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong in getCustomersBySpending():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getCustomersBySpending():");
                System.out.println(ex);
            }
            return customerSpenders;
        }
    }

    /**
     * Finds the top CustomerGenre's based on the invoices of a customer
     * @param customerId for a single customer
     * @return An ArrayList containing CustomerGenres
     */
    public ArrayList<CustomerGenre> getTopCustomerGenre(int customerId) {
        ArrayList<CustomerGenre> customerGenre = new ArrayList<>();
        ArrayList<CustomerGenre> largestGenres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement =
                    "SELECT Name, COUNT(GenreId) as numberOfInvoices " +
                    "FROM " +
                        "(SELECT CustomerId, i.InvoiceId, t.TrackId, g.GenreId, g.Name " +
                        "FROM " +
                        "    Invoice AS i " +
                        "    INNER JOIN " +
                        "    InvoiceLine AS il ON i.InvoiceId = il.InvoiceId " +
                        "    INNER JOIN " +
                        "    Track AS t ON il.TrackId = t.TrackId " +
                        "    INNER JOIN " +
                        "    Genre AS g ON t.GenreId = g.GenreId " +
                        "WHERE i.CustomerId = ?) " +
                    "GROUP BY Name " +
                    "ORDER BY numberOfInvoices DESC " ;


            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerGenre.add(new CustomerGenre(
                        resultSet.getString("name"),
                        resultSet.getInt("numberOfInvoices")
                ));
            }

            //Sorting out the top results
            int max = customerGenre.get(0).getNumberOfInvoices();
            for(CustomerGenre elem : customerGenre ) {
                int curr = elem.getNumberOfInvoices();
                if (curr != max) break;
                largestGenres.add(elem);
            }

        }
        catch (Exception ex){
            System.out.println("Something went wrong in getTopCustomerGenre():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getTopCustomerGenre():");
                System.out.println(ex);
            }
            return largestGenres;
        }
    }

    /**
     * Finds 5 random tracks and saves their names, artists and genres.
     *
     * @return an arrayList with 5 randomly selected tracks
     */
    @Override
    public ArrayList<Track> getHomePageContent() {
        ArrayList<Track> tracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT t.Name AS trackName, art.Name AS artistName, g.Name AS genreName " +
                    "FROM" +
                    "   Track AS t" +
                    "   INNER JOIN Genre AS g" +
                    "       ON t.GenreId = g.GenreId" +
                    "   INNER JOIN Album AS alb" +
                    "       ON t.AlbumId = alb.AlbumId" +
                    "   INNER JOIN Artist AS art" +
                    "       ON alb.ArtistId = art.ArtistId " +
                    "ORDER BY RANDOM() " +
                    "LIMIT 5";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Track(
                        resultSet.getString("trackName"),
                        resultSet.getString("artistName"),
                        resultSet.getString("genreName")
                ));
            }
        }
        catch (Exception ex){
            System.out.println("Something went wrong in getHomePageContent():");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception ex){
                System.out.println("Something went wrong while closing connection in getHomePageContent():");
                System.out.println(ex);
            }
            return tracks;
        }
    }

    /**
     * Finds a specific track and stores its name, artist and genre.
     *
     * @param trackName name of the track
     * @return a track with name, artist and genre
     */
    @Override
    public Track getTrackInfo(String trackName) {
        Track track = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been established.");

            String statement = "SELECT t.Name AS trackName, art.Name AS artistName, g.Name AS genreName " +
                    "FROM" +
                    "   Track AS t" +
                    "   INNER JOIN Genre AS g" +
                    "       ON t.GenreId = g.GenreId" +
                    "   INNER JOIN Album AS alb" +
                    "       ON t.AlbumId = alb.AlbumId" +
                    "   INNER JOIN Artist AS art" +
                    "       ON alb.ArtistId = art.ArtistId " +
                    "WHERE t.Name LIKE ?";
            PreparedStatement preparedStatement =
                    conn.prepareStatement(statement);
            preparedStatement.setString(1, "%"+trackName+"%");
            ResultSet resultSet = preparedStatement.executeQuery();

            track = new Track(
                    resultSet.getString("trackName"),
                    resultSet.getString("artistName"),
                    resultSet.getString("genreName")
            );
        } catch (Exception ex) {
            System.out.println("Something went wrong in getTrackInfo():");
            System.out.println(ex);
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection in getTrackInfo():");
                System.out.println(ex);
            }
            return track;
        }
    }
}
