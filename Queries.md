### Query 1

Read all the customers in the database, this should display their:
Id, first name, last name, country, postal code, phone number, email.

SELECT *
FROM Customer

### Query 2

Read a specific customer from the database (by Id), should display:
everything listed in Query 1

SELECT *
FROM Customer AS c
WHERE c.id = (INPUT ID FROM JAVA)

### Query 3

Read a specific customer by name, HINT: LIKE keyword can help for partial matches.

SELECT *
FROM Customer AS c
WHERE
c.firstName LIKE "(FNAME FROM JAVA)"
AND c.lastName LIKE "(LNAME FROM JAVA)"

### Query 4

OFFSET and LIMIT

SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email 
FROM customer
LIMIT ?
OFFSET ?

### Query 5

Add a new customer to the database. Customer contains all that is needed.

INSERT INTO Customers (FirstName, LastName, Country, PostalCode, Phone, Email) 
VALUES (?, ?, ?, ?, ?, ?)

### Query 6

Update an existing user

UPDATE Customer
SET FirstName = ?
WHERE CustomerId = ?

### Query 7

Return number of customers in each country, sort high -> low
First: get all the customers + countries, grouped by country.

SELECT SUM(Country) as numberOfCustomer, Country
FROM Customer c
GROUP BY Country
ORDER BY numberOfCustomers

### Query 8 

Return Customers, ordered by spending DESC (big -> smol)

SELECT ROUND(SUM(i.Total), 2) as totalSpent, CustomerId
FROM Invoice i NATURAL JOIN Customer c
GROUP BY CustomerId 
ORDER BY totalSpent DESC

### Query 9 

Find the most popular genre(s) for a single specific customer

SELECT Name, maxNumberOfInvoices
FROM 
(SELECT Name, MAX(numberOfInvoices) as maxNumberOfInvoices 
FROM (

    SELECT SUM(GenreId) as numberOfInvoices
    FROM 
        (SELECT CustomerId, InvoiceId, TrackId, GenreId, g.Name
        FROM 
            Invoice AS i 
            INNER JOIN 
            InvoiceLine AS il ON i.InvoiceId = il.InvoiceId 
            INNER JOIN 
            Track AS t ON il.TrackId = t.TrackId
            INNER JOIN
            Genre AS g ON t.GenreId = g.GenreId
        WHERE i.CustomerId = ?)
    GROUP BY Name
    ORDER BY numberOfInvoices DESC
)) AS COMPLICADO
WHERE maxNumberOfInvoices = COMPLICADO.numberOfInvoices

/*String statement =
"SELECT MAX(numberOfInvoices) AS maxNumberOfInvoices, Name " +
"FROM " +
"(SELECT COUNT(GenreId) as numberOfInvoices, Name " +
"FROM " +
"(SELECT CustomerId, i.InvoiceId, t.TrackId, g.GenreId, g.Name " +
"FROM " +
"    Invoice AS i " +
"    INNER JOIN " +
"    InvoiceLine AS il ON i.InvoiceId = il.InvoiceId " +
"    INNER JOIN " +
"    Track AS t ON il.TrackId = t.TrackId " +
"    INNER JOIN " +
"    Genre AS g ON t.GenreId = g.GenreId " +
"WHERE i.CustomerId = ?) " +
"GROUP BY Name " +
"ORDER BY numberOfInvoices DESC) " ;*/


### Query for home page
// 5 random artist
SELECT DISTINCT * 
FROM Artist
LIMIT 5



SELECT t.name, art.name, g.name
FROM 
    TRACK AS t 
    INNER JOIN Genre g
        ON t.genreId = g.genreId
    INNER JOIN Album alb
        ON t.albumId = alb.albumId
    INNER JOIN Artist art
        ON alb.artistId = art.artistId
ORDER BY RANDOM()
LIMIT 5













.