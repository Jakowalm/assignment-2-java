# The No Problem Project

Welcome to the "no problem" music database! The project uses 
[SQLite](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc)
to access the 
[Chinook](https://github.com/lerocha/chinook-database)
database.

## Thymeleaf

If the site is accessed without any parameters, the user is sent to a home page created in Thymeleaf.
The home page shows five randomly selected songs with their Names, Artist and Genre.
At teh top of the page, there is a search bar where a user can type in a song name and get information about it back.
As long as the name can be autocompleted to a song in the database, a result will be shown.
In order to make a new search, the user has to go back to the previous page in their browser.

If the name can't be found in the database, the site will show an error.
When this happens, the user can simply go back to the previous page and continue as normal.

## REST

The implementation features a RESTful API where information about customers can be fetched, added and modified in the database.

### REST API calls

- #### /api/customer/all
  - Find all the customers in the database.
- #### /api/customer/id/\<id>
  - Find a single customer by their id in the database.
- #### /api/customer/name/\<firstName>/\<lastName>
  - Find a single customer by their name in the database.
- #### /api/customer/all/offset/\<limit>/\<offset>
  - Find `limit` number of customer starting at index `offset`.
- #### /api/customer/insert/\<firstName>/\<lastName>/\<country>/\<postalCode>/\<phone>/\<email>
  - Adds a new customer to the end of the database.
- #### /api/customer/update/\<id>/\<firstName>/\<lastName>/\<country>/\<postalCode>/\<phone>/\<email>
  - Updates the customer with the correct `id`.
- #### /api/customer/countries
  - Find all the countries with customers in them, sorted by most customers to fewest.
- #### /api/customer/all/spending
  - Find all the customers in the database, sorted by the highest spenders.
- #### /api/customer/id/\<id>/genres
  - Finds the top genre of a single customer with the correct `id`.
